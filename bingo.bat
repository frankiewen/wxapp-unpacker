@echo off

setlocal EnableDelayedExpansion

set "WXAPPUNPACKER_PATH=%cd%"

set "FILE_FORMAT=wxapkg"


rem %1: pkg file or pkg dir; %2: order
 
if "%1" == "-d" (
  call :wxappUnpacker
) else (
  call :wxappUnpacker_pkg
)


:wxappUnpacker_pkg
  @REM "node %WXAPPUNPACKER_PATH%/wuWxapkg.js% %name%"
  node %WXAPPUNPACKER_PATH%\wuWxapkg.js %2 %1
goto:eof

:wxappUnpacker
  set de_dir=%1
  if "%1" == "" set de_dir="%cd%"

  echo "current path %de_dir%"

  for /f "delims=" %%i in ('dir /b /a-d /s "%de_dir%\*.%FILE_FORMAT%"') do (
    call :wxappUnpacker_pkg %%i %2
  )
goto:eof

pause